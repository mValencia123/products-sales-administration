package easysalesassistant.api.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ProviderDTO {
    private String description;
    private Long idTenant;
}
