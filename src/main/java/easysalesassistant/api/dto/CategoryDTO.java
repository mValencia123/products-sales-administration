package easysalesassistant.api.dto;

import lombok.Data;

@Data
public class CategoryDTO {
    String description;
    Long idTenant;
}
