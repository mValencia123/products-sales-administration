package easysalesassistant.api.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class StoreDTO {
    private String description;
    private Long idTenant;
}
